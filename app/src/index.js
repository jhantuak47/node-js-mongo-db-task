let express  = require('express');
const dotenv = require('dotenv');
let personRoute = require('./routes/person');
let customerRoute = require('./routes/customer');
let mongoose = require('mongoose');
let app = express();
let bodyParser = require('body-parser');
const path = require('path');
dotenv.config();



app.use(bodyParser.json());
app.use( (req,res, next) => {
    console.log(`${new Date().toString()} =>  ${req.originalUrl}` ,  req.body);
    next();
});

const server = process.env.HOST_NAME || 'localhost',
    database = process.env.DATABASE_NAME  || 'rest-api-workshop',
    username = process.env.USER_NAME || '',
    password =  process.env.PASSWORD || '';
mongoose.connect(`mongodb://${username}:${password}@${server}/${database}`, { useNewUrlParser: true });

// all route register here...
app.use(personRoute);
app.use('/api', customerRoute);
app.use(express.static('public'));

// handler for 404 - Resource Not Found..
app.use( (req, res, next) => {
    res.status(404).send('we think you are lost');
});
// handler for Error 500..
app.use( (err, req, res, next) => {
    console.error(err.stack);
    res.sendFile(path.join(__dirname, '../public/500.html'));
});
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.info(`Server has started on ${PORT}`));