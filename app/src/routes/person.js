let express = require('express');
let router = express.Router();

// QueryString => query property on the request object..
// localhost:3000/person?name=xyz&age=20
router.get('/person', (req, res) => {
    if(req.query.name){
        res.send(`You have requested a person, ${req.query.name}`);
    }else {
        res.send('You have requested a person');
    }
});
//params property on the request object...
// localhost:3000/person/xyz
router.get('/person/:name', (req, res) => {
    res.send(`You have requested a person', ${req.params.name}`);
});

// creating custom error..
router.get('/error', (req, res)=>{
    throw new Error('This is a forced error');
});
module.exports = router;