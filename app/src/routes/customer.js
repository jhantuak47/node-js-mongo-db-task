let CustomerModel = require('../models/customer.model');
let express = require('express');
let router = express.Router();

//create a new customer ..
router.post('/customer/create', (req, res,next) => {
    //req.body
    if(!req.body)
        return res.status(400).send('Request Body is missing !!');
    let Customer = new CustomerModel(req.body);
    Customer.save()
        .then( (customer) => {
            if(!customer || customer.length === 0)
                return res.status(500).send(customer);
            res.status(201).send(customer);
        })
        .catch( err => {
            res.status(500).json(err);
        });
});
// GETTING DATA
router.get('/customer', (req, res) => {
   if(!req.query.email)
       return res.status(400).send('Missing URL parameter email');
   CustomerModel.findOne({
       email:req.query.email,
   })
       .then(customer => {
           if(!customer)
               res.json({
                   success:false,
                   msg:'user does not exist'
               });
           res.json(customer)
       })
       .catch(err => {
           res.status(500).json(err);
       })
});
// UPDATE
router.put('/customer', (req, res) => {
    if(!req.query.email)
        return res.status(400).send('Missing URL parameter: email');
    CustomerModel.findOneAndUpdate({
        email:req.query.email,
    },req.body, {
        new:true
    })
        .then(customer => {
            if(!customer)
                res.json({
                    success:false,
                    msg:'user does not exist'
                });
            res.json(customer);
        })
        .catch(err => {
            res.status(500).json(err);
        })
});
// DELETE
router.delete('/customer', (req, res) => {
    if(!req.query.email)
        return res.status(400).send('Missing URL parameter: email');
    CustomerModel.findOneAndRemove({
        email:req.query.email,
    },req.body, {
        new:true
    })
        .then(customer => {
            if(!customer)
                res.json({
                    success:false,
                    msg:'user does not exist'
                });
            res.json(customer);
        })
        .catch(err => {
            res.status(500).json(err);
        })
});
module.exports = router;
