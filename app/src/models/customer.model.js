const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let customerSchema = new Schema({
    name:String,
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
    }
});
let Customer = mongoose.model('Customer', customerSchema);
module.exports = Customer;