# Node js/ Mongo DB Task.

Node js/ Mongo DB Task is a node.js example app which implements all CRUD operatio using MongoDB.

## Requirements

* Node 8
* Git
* mongoDB
* Express

## Common setup

Clone the repo and install the dependencies.

git clone https://jhantuak47@bitbucket.org/jhantuak47/node-js-mongo-db-task.git

~$ cd app

~$ npm install


## Create .env file

    * create .env file in your root directory(app).
    * copy paste the example.env file into your .env file.
    * Specify all your database credentials in your .env file.



## Run Application

Once setup is done head over to package.json file to run the application.

inside terminal

xyz/app ~$ npm run start-watch


## API's End Point.

    1. CREATE.

            http://xx.x.x:PORT/api/customer/create

                ->method :- POST
                ->body   :- {
                            	"name":"xyz",
                            	"email":"xyz",
                            	"address":"xyz",
                            }

    2. UPDATE

                    http://xx.x.x:PORT/api/customer

                    ->method :- PUT
                    ->body   :- {
                                   	"username":"xyz",
                                   	"password":"xyz",
                                    ....
                                }
                    ->Query Pramenter :- { "email": "xxxx"} <!-- pass email to delete the user --!>

    3. READ

                     http://xx.x.x.x:PORT/api/customer

                     -> method :- GET

                     -> Query Pramenter :- { "email": "xxxx"} <!-- pass email to delete the user --!>

    4. DELETE.

                    http://xx.x.x.x:PORT/api/customer

                     -> method :- DELETE

                     -> Query Pramenter :- { "email": "xxxx"} <!-- pass email to delete the user --!>

